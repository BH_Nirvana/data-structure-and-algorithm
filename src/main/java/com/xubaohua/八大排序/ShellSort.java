package com.xubaohua.八大排序;

/**
 * @program: 希尔排序
 * @description:
 * @author: 许宝华
 * @create: 2022-04-19 12:46
 */

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * 1、简单插入排序存在的问题：当需要插入的数是较小的数时，后移的次数明显增多，对效率有影响。
 * 2、希尔排序是希尔（Donald Shell）于1959年提出的一种排序算法。希尔排序也是一种插入排序，他是简单插入排序经过改进之后的一个
 * 更高效的版本，也称为缩小增量排序。
 * 3、希尔排序的基本思想：希尔排序是把记录按下标的一定增量分组，对每组使用直接插入排序算法排序，随着增量逐渐减少，
 * 每组包含的关键词越来越多，当增量减至1时，整个文件恰被分成一组，算法被终止。
 */
public class ShellSort {
    public static void main(String[] args) {
        int[] arr = new int[]{8, 9, 1, 7, 2, 3, 5, 4, 6, 0};
        System.out.println("排序前" + Arrays.toString(arr));

        int[] arrMax = new int[500000];
        for (int i = 0; i <500000 ; i++) {
            arrMax[i] = (int)(Math.random() * 8000000);
        }

        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String format = simpleDateFormat.format(date);
        System.out.println(format);

//        shellSortByExchange1(arr);
        shellSortByMove(arrMax);

        Date date1 = new Date();
        String format1 = simpleDateFormat.format(date1);
        System.out.println(format1);

    }


    /**
     * 希尔排序（移位法）对交换法的优化
     *
     * @param arr
     */
    private static void shellSortByMove(int[] arr) {

        //增量gap，并逐步的缩小增量
        for (int gap = arr.length / 2; gap > 0; gap /= 2) {
            //从第gap各元素，逐个对其所在的组进行直接插入排序
            for (int i = gap; i < arr.length; i++) {
                int j = i;
                int temp = arr[j];
                if (arr[j] < arr[j - gap]) {
                    while (j - gap >= 0 && temp < arr[j - gap]) {
                        arr[j] = arr[j - gap];
                        j -= gap;
                    }
                    //当退出while后，就说明当前temp找到了插入的位置
                    arr[j] = temp;
                }
            }
        }
//        System.out.println("排序后" + Arrays.toString(arr));

    }


    /**
     * 希尔排序（交换法）的推导过程,效率不高
     *
     * @param arr
     */
    private static void shellSortByExchange(int[] arr) {

        //第一轮,增量为length/2 10/2=5
        for (int i = 5; i < arr.length; i++) {
            //遍历数组中的所有元素（分5组，每组2个）
            for (int j = i - 5; j >= 0; j -= 5) {
                //如果当前元素大于加上步长后的那个元素，说明交换
                if (arr[j] > arr[j + 5]) {
                    int temp = 0;
                    temp = arr[j];
                    arr[j] = arr[j + 5];
                    arr[j + 5] = temp;
                }
            }
        }
        System.out.println("第一轮排序" + Arrays.toString(arr));

        //第二轮,增量为length/2 5/2=2
        for (int i = 2; i < arr.length; i++) {
            //遍历数组中的所有元素（分5组，每组2个）
            for (int j = i - 2; j >= 0; j -= 2) {
                //如果当前元素大于加上步长后的那个元素，说明交换
                if (arr[j] > arr[j + 2]) {
                    int temp = 0;
                    temp = arr[j];
                    arr[j] = arr[j + 2];
                    arr[j + 2] = temp;
                }
            }
        }
        System.out.println("第二轮排序" + Arrays.toString(arr));

        //第三轮,增量为length/2 2/2=1
        for (int i = 1; i < arr.length; i++) {
            //遍历数组中的所有元素（分5组，每组2个）
            for (int j = i - 1; j >= 0; j -= 1) {
                //如果当前元素大于加上步长后的那个元素，说明交换
                if (arr[j] > arr[j + 1]) {
                    int temp = 0;
                    temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
        System.out.println("第三轮排序" + Arrays.toString(arr));

    }

    /**
     * 希尔排序（交换法）优化
     *
     * @param arr
     */
    private static void shellSortByExchange1(int[] arr) {
        int temp = 0;
        for (int gap = arr.length / 2; gap > 0; gap /= 2) {
            for (int i = 1; i < arr.length; i++) {
                //遍历数组中的所有元素（分5组，每组2个）
                for (int j = i - gap; j >= 0; j -= gap) {
                    //如果当前元素大于加上步长后的那个元素，说明交换
                    if (arr[j] > arr[gap + j]) {
                        temp = arr[j];
                        arr[j] = arr[gap + j];
                        arr[gap + j] = temp;
                    }
                }
            }
        }
        System.out.println("第轮排序" + Arrays.toString(arr));
    }
}
