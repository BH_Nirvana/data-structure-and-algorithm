package com.xubaohua.JUC并发包.线程池;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @program: JavaSenior
 * @description:
 * @author: 许宝华
 * @create: 2022-06-26 11:19
 */

public class ExcutePool {
    public static void main(String[] args) {
        ExecutorService threadPool1 = Executors.newFixedThreadPool(5);
        ExecutorService threadPool2 = Executors.newSingleThreadExecutor();
        ExecutorService threadPool3 = Executors.newCachedThreadPool();

        // 是个顾客请求
        try{
            for (int i = 1; i <= 30; i++) {
                final int num = i;
                // 到此时执行execute()方法才创建线程
                threadPool3.execute(()->{
                    System.out.println(Thread.currentThread().getName() +"===>线程池"+num+ " 办理业务");
                });
            }
        }finally {
            // 关闭线程
            threadPool3.shutdown();
        }

    }
}
