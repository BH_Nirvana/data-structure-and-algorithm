package com.xubaohua.JUC并发包.线程池;

import java.util.concurrent.*;

/**
 * @program: JavaSenior
 * @description:
 * @author: 许宝华
 * @create: 2022-06-26 18:01
 */

public class FetureDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {


        ExecutorService threadPoolExecutor = new ThreadPoolExecutor(
                3,
                5,
                5L,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(5),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy()
        );

        long startTime = System.currentTimeMillis();

        System.out.println(startTime);
        FutureTask<String> stringFutureTask = new FutureTask<>(() -> {
            TimeUnit.MILLISECONDS.sleep(500);
            return "task1 over";
        });

        threadPoolExecutor.submit(stringFutureTask);
        FutureTask<String> stringFutureTask1 = new FutureTask<>(() -> {
            TimeUnit.MILLISECONDS.sleep(500);
            return "task2 over";
        });
        threadPoolExecutor.submit(stringFutureTask1);

        FutureTask<String> stringFutureTask2 = new FutureTask<>(() -> {
            TimeUnit.MILLISECONDS.sleep(500);
            return "task3 over";
        });
        threadPoolExecutor.submit(stringFutureTask2);

        System.out.println(stringFutureTask.get());
        System.out.println(stringFutureTask1.get());
        System.out.println(stringFutureTask2.get());

        long endTime = System.currentTimeMillis();
        System.out.println(endTime-startTime);
        threadPoolExecutor.shutdown();
    }
}
