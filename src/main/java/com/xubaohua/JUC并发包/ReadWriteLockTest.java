package com.xubaohua.JUC并发包;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @program: JavaSenior
 * @description:
 * @author: 许宝华
 * @create: 2022-06-25 16:21
 */

class MyCache{
    // 需要模仿从Map中取对象，所以先穿件一个map对象
    private volatile Map<String, Object> map = new HashMap<>();


    private ReadWriteLock rwLock = new ReentrantReadWriteLock();

    // 放数据
    public void put(String key, Object value) {
        try {
            rwLock.writeLock().lock();
            System.out.println(Thread.currentThread().getName()+"正在写操作"+key);
            // 暂停一会
            TimeUnit.MICROSECONDS.sleep(300);
            // 放数据
            map.put(key, value);
            System.out.println(Thread.currentThread().getName()+"写完了"+key);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            rwLock.writeLock().unlock();
        }
    }

    // 取数据
    public void get(String key) {
        try {
            rwLock.readLock().lock();
            System.out.println(Thread.currentThread().getName()+"正在取操作"+key);
            // 暂停一会
            TimeUnit.MICROSECONDS.sleep(300);
            // 放数据
            map.get(key);
            System.out.println(Thread.currentThread().getName()+"取完了"+key);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            rwLock.readLock().unlock();
        }
    }
}

public class ReadWriteLockTest {

    public static void main(String[] args) {


        MyCache myCache = new MyCache();
        for (int i = 1; i <= 6; i++) {
            final int num = i;
            new Thread(()->{
                myCache.put(String.valueOf(num),String.valueOf(num));
            },String.valueOf(i)).start();
        }

        for (int i = 1; i <= 6; i++) {
            final int num = i;
            new Thread(()->{
                myCache.get(String.valueOf(num));
            },String.valueOf(i)).start();
        }
    }
}

