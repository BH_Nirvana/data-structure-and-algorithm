package com.xubaohua.MapStructTest;

import lombok.Data;

/**
 * @program: JavaSenior
 * @description:
 * @author: 许宝华
 * @create: 2022-07-03 16:51
 */

@Data
public class UserRoleDTO {
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 用户名
     */
    private String name;
    /**
     * 角色名
     */
    private String roleName;

}
