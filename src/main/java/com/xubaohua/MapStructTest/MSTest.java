package com.xubaohua.MapStructTest;

import org.junit.Before;
import org.junit.Test;

/**
 * @program: JavaSenior
 * @description:
 * @author: 许宝华
 * @create: 2022-07-03 16:52
 */


public class MSTest {
    User user = null;

    /**
     * 模拟从数据库中查出user对象
     */
    @Before
    public void before() {
        Role role  = new Role(2L, "administrator", "超级管理员");
        user  = new User(1L, "zhangsan", "12345", "17677778888", "123@qq.com", role);
    }


    @Test
    public void test1(){
        UserRoleDTO userRoleDto = UserRoleMapper.INSTANCES.toUserRoleDto(user);
        System.out.println(userRoleDto);
    }

    @Test
    public void test2(){

    }
}
