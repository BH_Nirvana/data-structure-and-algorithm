package com.xubaohua.MapStructTest;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @program: JavaSenior
 * @description:
 * @author: 许宝华
 * @create: 2022-07-03 16:39
 */

@Data
@AllArgsConstructor
public class Role {
    private Long id;
    private String roleName;
    private String description;
}
