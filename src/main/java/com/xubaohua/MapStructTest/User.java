package com.xubaohua.MapStructTest;

import lombok.AllArgsConstructor;
import lombok.Data;
/**
 * @program: JavaSenior
 * @description:
 * @author: 许宝华
 * @create: 2022-07-03 16:38
 */


@Data
@AllArgsConstructor
public class User {

    private Long id;

    private String username;

    private String password;

    private String phoneNum;

    private String email;

    private Role role;

}
