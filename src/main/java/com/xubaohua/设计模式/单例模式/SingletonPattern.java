package com.xubaohua.设计模式.单例模式;

/**
 * @program: JavaSenior
 * @description:
 * @author: 许宝华
 * @create: 2022-06-23 22:41
 */

public class SingletonPattern {
    //防止内存指令重排
    private static volatile  SingletonPattern singletonPattern = null ;

    private SingletonPattern() {

    }

    public static SingletonPattern getSingletonPattern(){

        if(singletonPattern == null){
            singletonPattern =  new SingletonPattern();
        }
        return singletonPattern;
    }

    public static void main(String[] args) {


        SingletonPattern singletonPattern1 = SingletonPattern.getSingletonPattern();
        SingletonPattern singletonPattern2 = SingletonPattern.getSingletonPattern();

        System.out.println(singletonPattern1.hashCode());
        System.out.println(singletonPattern2.hashCode());
        System.out.println(singletonPattern1 == singletonPattern2);
    }
}
